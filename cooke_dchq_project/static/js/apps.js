//   apps.js

var USER_STATUS = window.sessionStorage.getItem('USER_STATUS');
var LOGIN_URL = window.sessionStorage.getItem('LOGIN_URL');

$(function () {

    /* Functions */

    var loadForm = function () {
        // console.log(USER_STATUS)
        if (USER_STATUS == 'true') {
            // console.log('logged in')
            var btn = $(this);
            $.ajax({
                url: btn.attr("data-url"),
                type: 'get',
                dataType: 'json',
                beforeSend: function () {
                    $("#modal-app").modal("show");
                },
                success: function (data) {
                    $("#modal-app .modal-content").html(data.html_form);
                }
            });
        } else {
            //not logged in, redirect to login page!
            // console.log('not logged in')
            bootbox.confirm({
                title: "Logged in?",
                message: "You need to be logged in to create new applications.",
                buttons: {
                    cancel: {
                        label: '<i class="fa fa-times"></i> Cancel'
                    },
                    confirm: {
                        label: '<i class="fa fa-check"></i> Continue to Login'
                    }
                },
                callback: function (result) {
                    if (result) {
                        login_redir()
                    }
                }
            });
            function login_redir() {
                window.location = LOGIN_URL;
            }

        }
    };

    var saveForm = function () {
        if (!$("#id_AppName, #id_appFqdn").not(".form-control-success").length) {
            // TODO relley not the best method, need to fix
            var form = $(this);
            $.ajax({
                url: form.attr("action"),
                data: form.serialize(),
                type: form.attr("method"),
                dataType: 'json',
                success: function (data) {
                    if (data.form_is_valid) {
                        var pathname = document.location.pathname;
                        // if not the dash then redirect
                        if (pathname != '/app/') {
                            window.location = '/app/'
                        }
                        $("#app_table tbody").html(data.html_book_list);  // <-- Replace the table body
                        $("#modal-app").modal("hide");  // <-- Close the modal
                    }
                    if (data.error) {
                        alert('job deployment error');
                    }
                    else {
                        $("#modal-app .modal-content").html(data.html_form);
                    }
                }
            });
            return false;
        }
        else {
            // send a alert
            $('<div class="alert alert-danger alert-dismissible fade show" role="alert"> <button type="button" class="close" data-dismiss="alert" aria-label="Close">    <span aria-hidden="true">&times;</span>  </button>  <strong>Holy guacamole!</strong> You should check in on some of those fields below.</div>').insertBefore("#AppName");
            // resets fields
            $('input#id_AppName').val("");
            $('input#id_appFqdn').val("");
            $('textarea#id_templateName').val("");
            //remove any help class
            $("#AppName").removeClass("has-success");
            $("#id_AppName").removeClass("form-control-success");
            $("#AppName").removeClass("has-danger");
            $("#id_AppName").removeClass("form-control-danger");
            $("#AppFqdn").removeClass("has-success");
            $("#id_appFqdn").removeClass("form-control-success");
            $("#AppFqdn").removeClass("has-danger");
            $("#id_appFqdn").removeClass("form-control-danger");

            return false; // blocks redirect after submission via ajax
        }
    };

    // var updateAppInfo = function () {
    //         $.ajax({
    //             url: '/app/info/id/' + value,
    //             type: 'GET',
    //             success: function (data) {
    //                 $("#appStatus_" + value).text(data['appStatus']);
    //                 $("#appFqdn_" + value).text(data['appFqdn']);
    //                 $("#appFqdnHttp_" + value).attr('href', 'http://' + data['appFqdn']);
    //             }
    //         });
    //     };

    var AppName_checker = function () {
        console.log('test');
        alert('test');
    };

    var appFqdn_checker = function () {
        console.log('test');
        alert('test');
    };

    /* App name and FQDN live check */

    $(document).on('input', '#id_AppName', function (event) {
        var text = $(this).val();
        // console.log(text);
        if (text.length >= 4) {
            // console.log('more then 4');
            $.ajax({
                url: '/app/check_app_detail/?appname=' + text,
                type: 'GET',
                success: function (data) {
                    // console.log(data);
                    if (data['name_exists'] == true) {
                        //remove any
                        $("#AppName").removeClass("has-success");
                        $("#id_AppName").removeClass("form-control form-control-success");
                        //add new
                        $("#AppName").addClass("has-danger");
                        $("#id_AppName").addClass("form-control form-control-danger");
                    }
                    else if (data['name_exists'] == false) {
                        // remove any
                        $("#AppName").removeClass("has-danger");
                        $("#id_AppName").removeClass("form-control form-control-danger");
                        // add new
                        $("#AppName").addClass("has-success");
                        $("#id_AppName").addClass("form-control form-control-success");
                        // $( '<div class="form-control-feedback">Success!</div>' ).after( "#id_AppName" );
                    }
                    else {
                        console.log('error');
                    }
                }
            });
        }
        else if (text.length <= 4) {
            //remove any
            $("#AppName").removeClass("has-success");
            $("#id_AppName").removeClass("form-control-success");
            $("#AppName").removeClass("has-danger");
            $("#id_AppName").removeClass("form-control-danger");
        }
    });

    $(document).on('input', '#id_appFqdn', function (event) {
        var text = $(this).val();
        // console.log(text);
        if (text.length >= 4) {
            // console.log('more then 4');
            $.ajax({
                url: '/app/check_app_detail/?appfqdn=' + text,
                type: 'GET',
                success: function (data) {
                    // console.log(data);
                    if (data['name_exists'] == true) {
                        //remove any
                        $("#AppFqdn").removeClass("has-success");
                        $("#id_appFqdn").removeClass("form-control form-control-success");
                        //add new
                        $("#AppFqdn").addClass("has-danger");
                        $("#id_appFqdn").addClass("form-control form-control-danger");
                    }
                    else if (data['name_exists'] == false) {
                        // remove any
                        $("#AppFqdn").removeClass("has-danger");
                        $("#id_appFqdn").removeClass("form-control form-control-danger");
                        // add new
                        $("#AppFqdn").addClass("has-success");
                        $("#id_appFqdn").addClass("form-control form-control-success");
                        // $( '<div class="form-control-feedback">Success!</div>' ).after( "#id_appFqdn" );
                    }
                    else {
                        console.log('error');
                    }
                }
            });
        }
        else if (text.length <= 4) {
            //remove any
            $("#AppFqdn").removeClass("has-success");
            $("#id_appFqdn").removeClass("form-control-success");
            $("#AppFqdn").removeClass("has-danger");
            $("#id_appFqdn").removeClass("form-control-danger");
        }
    });

    /* End App name and FQDN live check */

    /* Binding */

    // Create app
    $(".js-create-app").click(loadForm);
    $("#modal-app").on("submit", ".js-app-create-form", saveForm);

    // Delete app
    $("#app_table").on("click", ".js-delete-app", loadForm);
    $("#modal-app").on("submit", ".js-app-delete-form", saveForm);

    // make sure browser back reload the app list
    $(document).ready(function (e) {
        var $input = $('#refresh');

        $input.val() == 'yes' ? location.reload(true) : $input.val('yes');
        // console.log('reload')
    });

})
