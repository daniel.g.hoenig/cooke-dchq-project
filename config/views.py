# config/views.py
from django.shortcuts import get_object_or_404, render
from django.http import HttpResponseRedirect
from django.core.urlresolvers import reverse
from django.views import generic
from django.views.generic import ListView

from app.models import userCustomBlueprintTemplates


class TemplateListView(generic.ListView):
    template_name = 'pages/home.html'
    model = userCustomBlueprintTemplates

    def get_queryset(self):
        return userCustomBlueprintTemplates.objects.filter(templateVisibility__iexact='ENABLED')
