cooke-dchq-project
==================

A short description of the project.

.. image:: https://img.shields.io/badge/built%20with-Cookiecutter%20Django-ff69b4.svg
     :target: https://github.com/pydanny/cookiecutter-django/
     :alt: Built with Cookiecutter Django


:License: MIT


Settings
--------

Moved to settings_.

.. _settings: http://cookiecutter-django.readthedocs.io/en/latest/settings.html

Basic Commands
--------------

Setting Up Your Users
^^^^^^^^^^^^^^^^^^^^^

* To create a **normal user account**, just go to Sign Up and fill out the form. Once you submit it, you'll see a "Verify Your E-mail Address" page. Go to your console to see a simulated email verification message. Copy the link into your browser. Now the user's email should be verified and ready to go.

* To create an **superuser account**, use this command::

    $ python manage.py createsuperuser

For convenience, you can keep your normal user logged in on Chrome and your superuser logged in on Firefox (or similar), so that you can see how the site behaves for both kinds of users.

Test coverage
^^^^^^^^^^^^^

To run the tests, check your test coverage, and generate an HTML coverage report::

    $ coverage run manage.py test
    $ coverage html
    $ open htmlcov/index.html

Running tests with py.test
~~~~~~~~~~~~~~~~~~~~~~~~~~

::

  $ py.test

Live reloading and Sass CSS compilation
^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^^

Moved to `Live reloading and SASS compilation`_.

.. _`Live reloading and SASS compilation`: http://cookiecutter-django.readthedocs.io/en/latest/live-reloading-and-sass-compilation.html



Celery
^^^^^^

This app comes with Celery.

To run a celery worker:

.. code-block:: bash

    cd cooke_dchq_project
    celery -A cooke_dchq_project.taskapp worker -l info

Please note: For Celery's import magic to work, it is important *where* the celery commands are run. If you are in the same folder with *manage.py*, you should be right.





Sentry
^^^^^^

Sentry is an error logging aggregator service. You can sign up for a free account at  https://sentry.io/signup/?code=cookiecutter  or download and host it yourself.
The system is setup with reasonable defaults, including 404 logging and integration with the WSGI application.

You must set the DSN url in production.


Deployment
----------

The following details how to deploy this application.

Install git.
::
    sudo apt-get install git

Install Docker
::
    sudo apt-get update

    sudo apt-key adv --keyserver hkp://p80.pool.sks-keyservers.net:80 --recv-keys 58118E89F3A912897C070ADBF76221572C52609D

    sudo apt-add-repository 'deb https://apt.dockerproject.org/repo ubuntu-xenial main'

    sudo apt-get update

Make sure you are about to install from the Docker repo instead of the default Ubuntu 16.04 repo:
::
    apt-cache policy docker-engine

You should see output similar to the follow:

.. code-block:: bash

    docker-engine:
      Installed: (none)
      Candidate: 17.05.0~ce-0~ubuntu-xenial
      Version table:
         17.05.0~ce-0~ubuntu-xenial 500
            500 https://apt.dockerproject.org/repo ubuntu-xenial/main amd64 Packages
         17.04.0~ce-0~ubuntu-xenial 500
            500 https://apt.dockerproject.org/repo ubuntu-xenial/main amd64 Packages
         17.03.1~ce-0~ubuntu-xenial 500
            500 https://apt.dockerproject.org/repo ubuntu-xenial/main amd64 Packages
         17.03.0~ce-0~ubuntu-xenial 500
            500 https://apt.dockerproject.org/repo ubuntu-xenial/main amd64 Packages
         1.13.1-0~ubuntu-xenial 500
            500 https://apt.dockerproject.org/repo ubuntu-xenial/main amd64 Packages
         1.13.0-0~ubuntu-xenial 500
            500 https://apt.dockerproject.org/repo ubuntu-xenial/main amd64 Packages
         1.12.6-0~ubuntu-xenial 500
            500 https://apt.dockerproject.org/repo ubuntu-xenial/main amd64 Packages
         1.12.5-0~ubuntu-xenial 500
            500 https://apt.dockerproject.org/repo ubuntu-xenial/main amd64 Packages
         1.12.4-0~ubuntu-xenial 500
            500 https://apt.dockerproject.org/repo ubuntu-xenial/main amd64 Packages
         1.12.3-0~xenial 500
            500 https://apt.dockerproject.org/repo ubuntu-xenial/main amd64 Packages
         1.12.2-0~xenial 500
            500 https://apt.dockerproject.org/repo ubuntu-xenial/main amd64 Packages
         1.12.1-0~xenial 500
            500 https://apt.dockerproject.org/repo ubuntu-xenial/main amd64 Packages
         1.12.0-0~xenial 500
            500 https://apt.dockerproject.org/repo ubuntu-xenial/main amd64 Packages
         1.11.2-0~xenial 500
            500 https://apt.dockerproject.org/repo ubuntu-xenial/main amd64 Packages
         1.11.1-0~xenial 500
            500 https://apt.dockerproject.org/repo ubuntu-xenial/main amd64 Packages
         1.11.0-0~xenial 500
            500 https://apt.dockerproject.org/repo ubuntu-xenial/main amd64 Packages


Finally, install Docker:
::
    sudo apt-get install -y docker-engine

Docker should now be installed, the daemon started, and the process enabled to start on boot. Check that it's running:
::
    sudo systemctl status docker

Output

.. code-block:: bash

    docker.service - Docker Application Container Engine
       Loaded: loaded (/lib/systemd/system/docker.service; enabled; vendor preset: enabled)
       Active: active (running) since Mon 2017-06-05 16:44:11 PDT; 1min 24s ago
         Docs: https://docs.docker.com
     Main PID: 13761 (dockerd)
       CGroup: /system.slice/docker.service
               ├─13761 /usr/bin/dockerd -H fd://
               └─13791 docker-containerd -l unix:///var/run/docker/libcontainerd/docker-containerd.sock --metrics-interval=0 --start-timeout 2m --state-dir /var/run/docker/libcontainerd/containerd --shim docker-containerd-shim --runtime dock

        Jun 05 16:44:10 ubuntu-14 dockerd[13761]: time="2017-06-05T16:44:10.864302780-07:00" level=warning msg="Your kernel does not support swap memory limit"
        Jun 05 16:44:10 ubuntu-14 dockerd[13761]: time="2017-06-05T16:44:10.864356107-07:00" level=warning msg="Your kernel does not support cgroup rt period"
        Jun 05 16:44:10 ubuntu-14 dockerd[13761]: time="2017-06-05T16:44:10.864370663-07:00" level=warning msg="Your kernel does not support cgroup rt runtime"
        Jun 05 16:44:10 ubuntu-14 dockerd[13761]: time="2017-06-05T16:44:10.864828327-07:00" level=info msg="Loading containers: start."
        Jun 05 16:44:10 ubuntu-14 dockerd[13761]: time="2017-06-05T16:44:10.987656978-07:00" level=info msg="Default bridge (docker0) is assigned with an IP address 172.17.0.0/16. Daemon option --bip can be used to set a preferred IP address"
        Jun 05 16:44:11 ubuntu-14 dockerd[13761]: time="2017-06-05T16:44:11.030964439-07:00" level=info msg="Loading containers: done."
        Jun 05 16:44:11 ubuntu-14 dockerd[13761]: time="2017-06-05T16:44:11.054489884-07:00" level=info msg="Daemon has completed initialization"
        Jun 05 16:44:11 ubuntu-14 dockerd[13761]: time="2017-06-05T16:44:11.054534720-07:00" level=info msg="Docker daemon" commit=89658be graphdriver=aufs version=17.05.0-ce
        Jun 05 16:44:11 ubuntu-14 dockerd[13761]: time="2017-06-05T16:44:11.064957013-07:00" level=info msg="API listen on /var/run/docker.sock"
        Jun 05 16:44:11 ubuntu-14 systemd[1]: Started Docker Application Container Engine.


Docker
^^^^^^

See detailed `cookiecutter-django Docker documentation`_.

.. _`cookiecutter-django Docker documentation`: http://cookiecutter-django.readthedocs.io/en/latest/deployment-with-docker.html

Run your app with docker-compose
To get started, pull your code from source control (don’t forget the .env file) and change to your projects root directory.

You’ll need to build the stack first. To do that, run:
::
    docker-compose build
Once this is ready, you can run it with:
::
    docker-compose up
To run a migration, open up a second terminal and run:
::
    docker-compose run django python manage.py migrate
To create a superuser, run:
::
    docker-compose run django python manage.py createsuperuser
If you need a shell, run:
::
    docker-compose run django python manage.py shell

To get an output of all running containers.

To check your logs, run:
::
    docker-compose logs
If you want to scale your application, run:
::
    docker-compose scale django=4
    docker-compose scale celeryworker=2


