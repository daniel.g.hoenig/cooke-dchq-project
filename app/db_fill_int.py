# dc_fill_int.py

from app.models import dchqConnection, Blueprint
from app.forms import my_Form, UserForm
from dchq import Dchq

# get the endpoint details from the django db
x = dchqConnection.objects.get()  # (old) dchqConnection.objects.values_list('username')
# dchq connection string
d = Dchq({'username': x.username, 'password': x.password, 'endpoint': x.hostEndpoint})


# print(d.getBlueprints())

def containers_in_app(container_id):
    data = d.getAppById(container_id)
    list = []
    for i in range(0, (len(data['results']['containers']))):
        case = (data['results']['containers'][i]['containerName'])
        list.append(case)
    return list


def get_all_blueprint(*blueprintType):
    if 'dc' in blueprintType:
        blueprintType = 'DOCKER_COMPOSE'
    if 'vm' in blueprintType:
        blueprintType = 'VM_COMPOSE'
    if not blueprintType:
        blueprintType = 'all'
    data = d.getBlueprints('1', '100') # TODO: replace in the future with some logic for the page size. The "totalElements": 77, "totalPages": 1 element should do the trick.
    getList = []
    for i in range(0, (len(data['results']))):
        if (blueprintType in data['results'][i]['blueprintType']) and (blueprintType != 'all'):
            list_row = []
            for row in range(0, (len(data['results']))):
                bpId = (data['results'][i]['id'])
                bpNmae = (data['results'][i]['name'])
                bgTag = (data['results'][i]['tags'])
            list_row += [bpNmae, bpId, blueprintType, bgTag]
            getList.append(list_row)
        if blueprintType == 'all':
            list_row = []
            for row in range(0, (len(data['results']))):
                bpId = (data['results'][i]['id'])
                bpNmae = (data['results'][i]['name'])
                bpType = (data['results'][i]['blueprintType'])
                bgTag = (data['results'][i]['tags'])
            list_row += [bpNmae, bpId, bpType, bgTag]
            getList.append(list_row)

    return getList

# Now to use it:
blueprints_list = get_all_blueprint('dc')
blueprints = Blueprint.objects.create_from_list(blueprints_list)
# print(blueprints)
print('blueprints loaded/updated')

# new_blueprints_li
# st = Blueprint.objects.last()
# blueprints_list == new_blueprints_list (although possibly in different order)
# full_blueprints_list = Blueprint.objects.as_list()
# filtered_blueprints_list = Blueprint.objects.filter(name__startswith='t5').as_list()

# test_blue = Blueprint.objects.all()
# print(test_blue)


# print(new_blueprints_list)