# import rest_framework
from rest_framework import serializers

from .models import userApps


class userAppSerializer(serializers.ModelSerializer):
    class Meta:
        model = userApps
        fields = ('id',
                  'APPuid',
                  'AppName',
                  'owner',
                  'blueprintUid',
                  'tags',
                  'buildTime',
                  'lastMod',
                  'appFqdn',
                  'appStatus')
