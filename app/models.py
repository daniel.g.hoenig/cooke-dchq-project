# app/models.py

# from __future__ import absolute_import, unicode_literals
from django.db import models


# Create your models here.

# Create a new queryset for our Blueprint manager
class BlueprintQuerySet(models.QuerySet):
    def as_list(self):
        """
        Return a list of lists containing Blueprint attributes
        """
        # Use list comprehension to return a list of lists; create the inner
        # lists by calling Blueprint.as_list on each Blueprint instance
        return [blueprint.as_list() for blueprint in self]


# Create a new model manager for our Blueprint model
class BlueprintManager(models.Manager):
    # def get_queryset(self):
    #     # Replace the default queryset with our one which can generate lists
    #     return BlueprintQuerySet(self.model, using=self._db)

    # 2016-12-31 edit: changed create_from_list to return queryset
    def create_from_list(self, blueprints_list):
        """
        Creates Blueprint instances from a list of (name, uid) pairs

        Returns a queryset of Blueprint instances
        """
        # Empty array to keep track of blueprints
        blueprint_pks = []

        # Step through the list
        # At the same time unpack the inner list into two variables
        for name, uid, type, tags in blueprints_list:
            # "self" here is a reference to the BlueprintManager instance, ie
            # Blueprint.objects (we'll assign it further down).
            #
            # get_or_create returns the object and a boolean for whether it was
            # created or loaded. We'll identify it by the uid, and if it
            # doesn't exist we'll create it with the name from the list
            blueprint, created = self.get_or_create(
                uid=uid, defaults={'name': name, 'type': type, 'tags': tags}
            )

            if not created and blueprint.name != name:
                # It already existed - but with the wrong name. Update.
                blueprint.name = name
                blueprint.save()

            # Keep track of blueprints we created or loaded
            blueprint_pks.append(blueprint.pk)

        # 2016-12-31 edit: make this return a queryset instead of list
        # Turn our list of blueprints into a Blueprint queryset
        blueprints = self.filter(pk__in=blueprint_pks)
        return blueprints


# Rename the class to something shorter, and use singular names not plural.
# If you put this in an app called "dchq_server", and this will show in the
# admin as "Dchq server > Blueprints".
# Note that python style guide is CamelCase for class names, so if you want the
# full name, it should be "DchqServerBlueprint"
class Blueprint(models.Model):
    # These fields are properties of the Blueprint, so they don't really need
    # a "blueprint" prefix. If you do want a prefix though, follow python
    # standards and call them "blueprint_name" rather than "blueprintName" -
    # python style guide is snake_case for variables, attributes and methods.
    # See https://www.python.org/dev/peps/pep-0008/#descriptive-naming-styles
    name = models.CharField(max_length=255)
    uid = models.CharField(max_length=255, unique=True)
    type = models.CharField(max_length=255, default='')
    tags = models.CharField(max_length=255, null=True)
    visibility = models.CharField(max_length=255, null=True, default='')

    # Now instantiate our new manager to replace the default one
    # 2016-12-31 edit: changed this line so that the manager gets queryset methods
    objects = BlueprintManager.from_queryset(BlueprintQuerySet)()

    # Str must return a string, you can't return a list.
    # Use something meaningful to humans - this will show up all over the admin
    def __str__(self):
        return self.name
        # Alternatively you could show both fields with something like
        # return '{} ({})'.format(self.name, self.uid)

    # If you want to get back your data in its original format, create a
    # separate method
    def as_list(self):
        """
        Return a list of blueprint attributes
        """
        return [self.name, self.uid, self.type, self.tags, self.visibility]


class userApps(models.Model):
    APPuid = models.CharField(max_length=255)
    AppName = models.CharField(max_length=255)
    owner = models.CharField(max_length=255)
    blueprintUid = models.CharField(max_length=255)
    blueprintYAML = models.CharField(max_length=2048)
    tags = models.CharField(max_length=255, null=True)
    buildTime = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    lastMod = models.DateTimeField(auto_now_add=True, null=True, blank=True)
    appStatus = models.CharField(max_length=255)
    appLocalFqdn = models.CharField(max_length=255)
    appFqdn = models.CharField(max_length=255)

    # def __str__(self):
    #     return [self.APPuid, self.owner, self.blueprintUid, self.tags, self.buildTime, self.lastMod]

    def __str__(self):
        return self.AppName

    def get_fields_and_values(self):
        return [(field, field.value_to_string(self)) for field in userApps._meta.fields]


class userCustomBlueprintTemplates(models.Model):
    templateName = models.CharField(max_length=255)
    templateVisibility = models.CharField(max_length=255, null=True, default='')
    templateYAML = models.TextField(max_length=2048)
    templateIcon = models.CharField(max_length=255, null=True, default='fa-user')

    def __str__(self):
        return self.templateName


class dchqConnection(models.Model):  # model - class    - table
    username = models.CharField(max_length=255, default='admin@dchq.io', blank=True,
                                null=True)  # field - instance - row
    password = models.CharField(max_length=255, default='admin123', blank=True, null=True)
    hostEndpoint = models.CharField(max_length=255, default='192.168.1.180', blank=True, null=True)

    def __str__(self):
        return '%s %s %s' % (self.username, self.password, self.hostEndpoint)  # update, this works better
        # return localize(self.username, self.password, self.hostEndpoint)  # this one causes an error
