from django.conf.urls import url, include
from . import views
# from dchq_deploy.views import taskstatus
from django.conf import settings

urlpatterns = [
    url(r'^$', views.DashboardPage.as_view(template_name='app/app_list.html'), name='dashboard'),
    url(r'^poll_state$', views.poll_state,name='poll_state'),
    url(r'^create/(?P<pk>[^/]+)?$', views.app_create, name='app_create'),
    url(r'^create/$', views.app_create, name='app_create'),
    url(r'^(?P<pk>\d+)/delete/$', views.app_delete, name='app_delete'),
    url(r'^info/id/(?:(?P<pk>[^/]+))?/$', views.app_detail, name='app_info_poll'),
    url(r'^check_app_detail/$', views.check_app_detail, name='check_app_detail')
]


if settings.DEBUG:
    import debug_toolbar

    urlpatterns += [
        url(r'^__debug__/', include(debug_toolbar.urls)),
    ]
