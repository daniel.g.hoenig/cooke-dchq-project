from __future__ import absolute_import, unicode_literals
import sys

from app.dchq.dchq import Dchq

# sys.path.append('/home/daniel/Projects/DCHQ-Python-SDK/dchq')

from datetime import datetime
import json

import time
# from app.models import userApps, userCustomBlueprintTemplates
import math

d = Dchq({'username': 'admin@dchq.io', 'password': 'admin123', 'endpoint': '192.168.1.169'})

# print(d.getBlueprints('1', '100')) # 'page', 'pageSize
'''
print d.createBlueprint({
"name": "amal-Nginx-test",
"blueprintType": "DOCKER_COMPOSE",
"version": "2.0",
"yml": ""
});
print d.getBlueprintsManage();
print d.getBlueprintsManageById('4028818650d4aca10150d4bf63470003');
print d.searchBlueprintsLibraryPage({'q': 'amal'});
print d.getBlueprintById('2c91808651a95c4d0151da0e9b087679');
print d.updateBlueprintById('2c91808651a95c4d0151da0e9b087679', {
	'description': 'Created by amal for testing',
	'name': 'amal-Nginx-test-again-again',
	"blueprintType": "DOCKER_COMPOSE",
	"version": "2.0",
	"yml": ""
});
print d.deleteBlueprintById('2c91808651a95c4d0151da0e9b087679');
print d.starBlueprintById('2c91808651a95c4d0151da0e9b087679');
print d.unstarBlueprintById('2c91808651a95c4d0151da0e9b087679');
print d.getStarredBlueprints();
print d.getBlueprintYamlById('402881864e1a36cc014e1a399cf90101');

print d.getDatacenters();
print d.getDatacenterById('2c91808651a95c4d0151c4cfabf21cb3');
print d.createDatacenter({
	'name': 'amal-test-datacenter'
});
print d.updateDatacenterById('2c91808651a95c4d0151dc98f37a0548', {
	'name': 'amal-test-datacenter-edited-python'
});
print d.deleteDatacenterById('2c91808651a95c4d0151c4cfabf21cb3');
print d.getDatacentersManage();
print d.searchDatacenters({'q': 'amal'});

print d.getBuilds();
print d.getBuildsManage();
print d.getBuildsManageById('addsaf');
print d.searchBuilds({'q': 'amal'});
print d.searchActiveEntitled({'q': 'amal'});
print d.reindexBuilds();
print d.getBuildById('adsa');
print d.createBuild({'name': 'amal-test-build', 'repository': 'dsfs', 'tag': 'test'});
print d.buildNow({'name': 'amal-test-build', 'repository': 'dsfs', 'tag': 'test'});
print d.updateBuildById('adsfdsf', {'name': 'amal-test-build-edited', 'repository': 'dsfs', 'tag': 'test'});
print d.deleteBuildById('sdkfmk');

print d.getDockerservers();
print d.getDockerserversManage();
print d.getDockerserverById('asda');
print d.getDockerserverStatusById('asda');
print d.getDockerserverPingById('asda');
print d.createDockerserver({
    "name": "kjwdnfkjs",
    "hostOrIp": "akjnf",
    "description": "lnsdknfkl",
    "size": 1,
    "inactive": False,
    "dataCenter": {}
});;
print d.updateDockerserverById('asdasdhj', {
    "name": "kjwdnfkjs",
    "hostOrIp": "akjnf",
    "description": "lnsdknfkl",
    "inactive": False
});
print d.deleteDockerserverById('asda');

print d.getApps();
print d.getAppById('dsafds');
print d.getActiveApps();
print d.getDestroyedApps();
print d.deleteAllDestroyedApps();
print d.updateAppById('adas', {});
print d.deployAppById('dsafds');
print d.getAppsDeploy();
print d.stopAppById('sdfs');
print d.startAppById('sdfs');
print d.restartAppById('sdfs');
print d.destroyAppById('sdfds');
print d.getAppBackupsById('dfsfd');
print d.createAppBackupById('sgffd');
print d.getAppPluginsById('fsdf');
print d.createAppPluginById('sdfs', {'continuous': True, 'note': 'aaf'});
print d.createAppPluginNowById('sdfs', {'continuous': True, 'note': 'aaf'});
print d.appRollbackById('dsfs');
print d.appRollbackNowById('sdfs', {'continuous': True, 'note': 'aaf'});
print d.createAppScaleOutCreateById('sdfs');
print d.getAppScaleInCreateById('sdfs');
print d.createAppScaleInById('sdfs');
print d.createAppScaleInNowById('sdfsdf');
print d.searchApps({'q': 'sadfdsf'});
print d.appMonitorById('adsas');

print d.getUsergroups();
print d.getUsergroupById('2c91808651a95c4d0151ea023320555b');
print d.createUsergroup({'name': 'sadfdsf'});
print d.updateUsergroupById('2c91808651a95c4d0151ea023320555b', {'name': 'amal'});
print d.deleteUsergroupById('2c91808651a95c4d0151ea023320555b');
print d.searchUsergroups({'q': 'sadfdsf'});

print d.getProfiles();
print d.getProfileById('dsgfdg');
print d.updateProfileById('dsgfdg', {'name': 'amal'});
print d.deleteProfileById('dsgfdg');
print d.createProfile({'name': 'amal', 'defaultProfile': True});
print d.searchProfiles({'q': 'amal'});

print d.searchMessages({'q': 'amal'});
print d.getUnreadMessages();
print d.getOpenMessages();
print d.getMessageById('sfds');
print d.archiveMessageById('sfds');
print d.getArchivedMessages();
print d.deleteMessageById('sdfsdf');

print d.getUsers();
print d.getUserById('dsfds');
print d.updateUserById('sfds', {
	'firstname': 'dfsdf'
});
print d.deleteUserById('dsfsd');
print d.createUser({
    "username": "amal-test",
    "password": "password",
    "email": "amal@amal.com",
    "firstname": "amal",
    "lastname": "francis",
    "enabled": True,
    "company": "amal",
    "jobTitle": "amal",
    "phoneNumber": "1234567890"
});
print d.searchUsers({'q': 'amal'});
print d.updateUserProfileById('2c91808651a95c4d0151f2c0aaa611d9', {
    "company": "amal company"
});
print d.getUserLoginStatus();
print d.signupUser({
    "username": "amal-test",
    "password": "password",
    "email": "amal@amal.com",
    "firstname": "amal",
    "lastname": "francis",
    "company": "amal"
});
print d.signupTenantUser({
    "username": "amal-test",
    "password": "password",
    "email": "amal@amal.com",
    "firstname": "amal",
    "lastname": "francis",
    "company": "amal"
});
print d.searchUserByEmail('amalfra@gmail.com');
print d.generateUserPasswordResetByEmail('amalfra@gmail.com');
print d.resetUserPasswordResetById('hjsdfhd', 'password');

print d.createPlugin({
    "name": "amal-test",
    "version": "1.0",
    "license": "MIT",
    "description": "amal test",
		"baseScript": 'test'
});
print d.getPluginsManage();
print d.getPluginManageById('safd');
print d.getPluginsStarred();
print d.updatePluginById('2c91808651a95c4d0151f318c64f1444', {
    "name": "amal-test updated"
});
print d.deletePluginById('2c91808651a95c4d0151f318c64f1444');
print d.searchPlugins({'q': 'amal'});
print d.reindexPlugins();
print d.searchActiveEntitledPlugins({'q': 'amal'});
print d.starPluginById('2c91808651a95c4d0151f318c64f1444');
print d.unstarPluginById('2c91808651a95c4d0151f318c64f1444');
print d.getPlugins();

print d.getCloudproviderRegionsById('fsd');
print d.getCloudproviderMachinetypesById('sdkjfn');
print d.getCloudproviderMachineimagesByIdRegion('sdkjfn', 'sadfdsf');
print d.getCloudproviderNetworksByIdRegion('sdkjfn', 'sadfdsf');
print d.getCloudproviderSecuritygroupsByIdRegion('sdkjfn', 'sadfdsf');

print d.getRegistryaccounts();
print d.getRegistryaccountById('asda');
print d.deleteRegistryaccountById('asda');
print d.getRegistryaccountsManage();
print d.searchRegistryaccounts({'q': 'amal'});
print d.reindexRegistryaccounts();
print d.getRegistryaccountJenkinsById('dsfsdf');
print d.getRegistryaccountTypeById('sdgdf');
print d.getRegistryaccountCloudproviders();
print d.createRegistryaccount({'name': 'amal-test'});
print d.updateRegistryaccountById('2c91808651a95c4d0151f7398ab92c80', 
	{'name': 'amal-test renamed'});

print d.settingsEncrypt('dsfdsf', 'sdfdsf');'''


# use to conver output to json, paste to http://jsoneditoronline.org/
# print(json.dumps(d.getAppById('2c91808657fa2f18015812ce0bfd260c'), indent=4, sort_keys=True))

# print(json.dumps(d.getAppById('2c91808657fa2f18015812ce0bfd260c'), sort_keys=True))


# data = d.getAppById('2c91808658f6f1e70158fa1805f5042b')


# print(data)
# print(data['results']['containers'][0]['containerName'])


# using len to gen the top range of the keys http://stackoverflow.com/a/4582474
# for i in range(0, (len(data['results']['containers']))):
#     print(data['results']['containers'][i]['containerName'])

def containers_in_app(container_id):
    data = d.getAppById(container_id)
    list = []
    for i in range(0, (len(data['results']['containers']))):
        case = (data['results']['containers'][i]['containerName'])
        list.append(case)
    return list


# print(containers_in_app('2c91808658f6f1e70158fa1805f5042b'))


# print(json.dumps(d.deployAppById('2c91808656b597ff0156b5d9f1ed0042'), indent=4, sort_keys=True))

# print(d.getAppById('2c91808657fa2f18015812ce0bfd260c'))

# print(d.settingsRecreateMissingQueues());


def get_all_blueprint():
    data = d.getBlueprints('1',
                           '100')  # TODO: replace in the future with some logic for the page size. The "totalElements": 77, "totalPages": 1 elealment should do the trick.
    list = []
    for i in range(0, (len(data['results']))):
        list_row = []
        for row in range(0, (len(data['results']))):
            bpId = (data['results'][i]['id'])
            bpNmae = (data['results'][i]['name'])
        list_row += [bpNmae, bpId]
        list.append(list_row)

    return list


# print(get_all_blueprint())
# print(json.dumps(d.getBlueprints('1', '100'), indent=4, sort_keys=True));

def test(*blueprintType):
    if 'dc' in blueprintType:
        blueprintType = 'DOCKER_COMPOSE'
    if 'vm' in blueprintType:
        blueprintType = 'VM_COMPOSE'
    if not blueprintType:
        blueprintType = 'all'
    data = d.getBlueprints('1', '100')
    list = []
    for i in range(0, (len(data['results']))):
        if (blueprintType in data['results'][i]['blueprintType']) and (blueprintType != 'all'):
            list_row = []
            for row in range(0, (len(data['results']))):
                bpId = (data['results'][i]['id'])
                bpNmae = (data['results'][i]['name'])
                bgTag = (data['results'][i]['tags'])
            list_row += [bpNmae, bpId, blueprintType, bgTag]
            list.append(list_row)
        if blueprintType == 'all':
            list_row = []
            for row in range(0, (len(data['results']))):
                bpId = (data['results'][i]['id'])
                bpNmae = (data['results'][i]['name'])
                bpType = (data['results'][i]['blueprintType'])
                bgTag = (data['results'][i]['tags'])
            list_row += [bpNmae, bpId, bpType, bgTag]
            list.append(list_row)

    return list


# print(json.dumps(test('dc'), indent=4, sort_keys=True)); # 'VM_COMPOSE'(vm) or 'DOCKER_COMPOSE'(dc) or blank for all types

def deploy_contaner_blueprint(blueprintUID):
    data = d.deployAppById(blueprintUID);
    return data


# print(json.dumps(deploy_contaner_blueprint('2c91808656b597ff0156b5d9f1ed0042'), indent=4, sort_keys=True));  #make a new contaner

# print(json.dumps(d.getAppById('2c918086591e503801597842adb93668'), indent=4, sort_keys=True)) #get the new contaners


def deploy_blueprint(uid, contanerOwner):
    # call the deploy app function
    postResult = d.deployAppById(uid)
    print(json.dumps(postResult, indent=4, sort_keys=True)) # debug
    # create an empty dir
    list = []
    # check for any error in the reply
    # check to see if an error message was returned
    if 'ERROR' == postResult["messages"][0]["messageType"]:
        # gracefully fail if an error message is returned
        error = postResult
        return error
    else:
        for i in range(0, (len(postResult['results']))):
            list_row = []
            # contanerOwner = 'admin'
            contanterCreateTime = str(datetime.now())
            for row in range(0, (len(postResult['results']))):
                bpId = (postResult['results']['id'])
                bpProvisionState = (postResult['results']['provisionState'])
            list_row += [bpId, bpProvisionState, contanerOwner, contanterCreateTime]
            list.append(list_row)

            return list


# print(json.dumps(deploy_blueprint('2c9180865c418284015c4e021de2099e', 'admin'), indent=4, sort_keys=True))
# print(deploy_blueprint('2c91808656b597ff0156b5d9f1ed0042', 'admin'))


def deployed_contaner_status(uid):
    #  call the update dchq function
    updateResult = d.getAppById(uid)
    # print(updateResult)
    # create an empty dir
    list = []
    # check for any error in the reply
    if updateResult['errors'] != 'false':

        # check to make sure that the app is running, got to check all the states to pervent getting an error
        if updateResult['results']['provisionState'] == 'RUNNING':
            # if the provisionState is running check to see if all the containers are running
            list = []
            for i in range(0, (len(updateResult['results']['containers']))):
                # if everything in the container is running, return that fact
                if updateResult['results']['containers'][i]['containerExpectedStatus'] == 'RUNNING' and \
                        updateResult['results']['containers'][i]['containerStatus'] == 'RUNNING':
                    list_row = []
                    case = updateResult['results']['containers'][i]['containerId']
                    containerName = updateResult['results']['containers'][i]['containerName']
                    containerStatus = updateResult['results']['containers'][i]['containerStatus']
                    list_row += [case, containerName, containerStatus]
                    list.append(list_row)
                # if everything in the container is stoped, return that fact
                if updateResult['results']['containers'][i]['containerExpectedStatus'] == 'RUNNING' and \
                        updateResult['results']['containers'][i]['containerStatus'] == 'STOPPED':
                    list_row = []
                    case = updateResult['results']['containers'][i]['containerId']
                    containerName = updateResult['results']['containers'][i]['containerName']
                    containerStatus = updateResult['results']['containers'][i]['containerStatus']
                    list_row += [case, containerName, containerStatus]
                    list.append(list_row)
                if updateResult['results']['containers'][i]['containerExpectedStatus'] == 'STOPPED' and \
                        updateResult['results']['containers'][i]['containerStatus'] == 'RUNNING':
                    list_row = []
                    case = updateResult['results']['containers'][i]['containerId']
                    containerName = updateResult['results']['containers'][i]['containerName']
                    containerStatus = updateResult['results']['containers'][i]['containerStatus']
                    list_row += [case, containerName, containerStatus]
                    list.append(list_row)
                if updateResult['results']['containers'][i]['containerExpectedStatus'] == 'STOPPED' and \
                        updateResult['results']['containers'][i]['containerStatus'] == 'STOPPED':
                    list_row = []
                    case = updateResult['results']['containers'][i]['containerId']
                    containerName = updateResult['results']['containers'][i]['containerName']
                    containerStatus = updateResult['results']['containers'][i]['containerStatus']
                    list_row += [case, containerName, containerStatus]
                    list.append(list_row)
            return list
        if updateResult['results']['provisionState'] == 'STOPPED':
            # if the provisionState is running check to see if all the containers are running
            list = []
            for i in range(0, (len(updateResult['results']['containers']))):
                # if everything in the container is running, return that fact
                if updateResult['results']['containers'][i]['containerExpectedStatus'] == 'RUNNING' and \
                        updateResult['results']['containers'][i]['containerStatus'] == 'RUNNING':
                    list_row = []
                    case = updateResult['results']['containers'][i]['containerId']
                    containerName = updateResult['results']['containers'][i]['containerName']
                    containerStatus = updateResult['results']['containers'][i]['containerStatus']
                    list_row += [case, containerName, containerStatus]
                    list.append(list_row)
                # if everything in the container is stoped, return that fact
                if updateResult['results']['containers'][i]['containerExpectedStatus'] == 'RUNNING' and \
                        updateResult['results']['containers'][i]['containerStatus'] == 'STOPPED':
                    list_row = []
                    case = updateResult['results']['containers'][i]['containerId']
                    containerName = updateResult['results']['containers'][i]['containerName']
                    containerStatus = updateResult['results']['containers'][i]['containerStatus']
                    list_row += [case, containerName, containerStatus]
                    list.append(list_row)
                if updateResult['results']['containers'][i]['containerExpectedStatus'] == 'STOPPED' and \
                        updateResult['results']['containers'][i]['containerStatus'] == 'STOPPED':
                    list_row = []
                    case = updateResult['results']['containers'][i]['containerId']
                    containerName = updateResult['results']['containers'][i]['containerName']
                    containerStatus = updateResult['results']['containers'][i]['containerStatus']
                    list_row += [case, containerName, containerStatus]
                    list.append(list_row)
            return list
        if updateResult['results']['provisionState'] == 'PROVISIONING':
            # if the provisionState is running check to see if all the containers are running
            list = []
            for i in range(0, (len(updateResult['results']['containers']))):
                # if everything in the container is running, return that fact
                if updateResult['results']['containers'][i]['containerExpectedStatus'] == 'RUNNING' and \
                        updateResult['results']['containers'][i]['containerStatus'] == 'RUNNING':
                    list_row = []
                    case = updateResult['results']['containers'][i]['containerId']
                    containerName = updateResult['results']['containers'][i]['containerName']
                    containerStatus = updateResult['results']['containers'][i]['containerStatus']
                    list_row += [case, containerName, containerStatus]
                    list.append(list_row)
                # if everything in the container is stoped, return that fact
                if updateResult['results']['containers'][i]['containerExpectedStatus'] == 'RUNNING' and \
                        updateResult['results']['containers'][i]['containerStatus'] == 'STOPPED':
                    list_row = []
                    case = updateResult['results']['containers'][i]['containerId']
                    containerName = updateResult['results']['containers'][i]['containerName']
                    containerStatus = updateResult['results']['containers'][i]['containerStatus']
                    list_row += [case, containerName, containerStatus]
                    list.append(list_row)
                if updateResult['results']['containers'][i]['containerExpectedStatus'] == 'STOPPED' and \
                        updateResult['results']['containers'][i]['containerStatus'] == 'STOPPED':
                    list_row = []
                    case = updateResult['results']['containers'][i]['containerId']
                    containerName = updateResult['results']['containers'][i]['containerName']
                    containerStatus = updateResult['results']['containers'][i]['containerStatus']
                    list_row += [case, containerName, containerStatus]
                    list.append(list_row)
            return list
        if updateResult['results']['provisionState'] == 'ERRORS':
            # fail if the app is in an error state
            list_row = []
            appName = updateResult['results']['name']
            appStatus = updateResult['results']['provisionState']
            list_row += [appName, uid, appStatus]
            list.append(list_row)
            return list
        if updateResult['results']['provisionState'] == 'PROVISIONING_FAILED':
            error = 'app stuck in PROVISIONING_FAILED'
            # print(error)
            return error

    else:
        error = 'Error checking container status'
        print(error)
        return error


# print(json.dumps(deployed_contaner_status('2c9180865ae25e7b015aef9bc1e003ae'), indent=4, sort_keys=True))
# print(deployed_contaner_status('2c9180865a969c44015a98564228015d'))


def deployed_contaner_check(uid, restartTry):
    # debug
    # print(uid)

    # get the state of the running sub containers in the app
    stateResults = deployed_contaner_status(uid)

    # debug
    # print(stateResults)
    # print(json.dumps(stateResults, indent=4, sort_keys=True))
    if stateResults != 'app stuck in PROVISIONING_FAILED':
        for i in range(0, (len(stateResults))):
            if stateResults[i][2] == 'STOPPED':
                return 'The app is stopped'
            if stateResults[i][2] == 'RUNNING':
                return 'The app is running'
                # return stateResults
            if stateResults[i][2] == 'RUNNING' and stateResults[i][2] == 'STOPPED':
                return 'The app is stopped'
                # return stateResults
    else:
        return 'app stuck in PROVISIONING_FAILED'


# print(json.dumps(d.restartAppById('2c9180865ae25e7b015aef9bc1e003ae'), indent=4, sort_keys=True))
# print(json.dumps(deployed_contaner_check('2c9180865ae25e7b015aef9bc1e003ae', 'true'), indent=4, sort_keys=True))

def contaner_status_check(uid, restartTry):
    # debug
    # print(uid)

    # get the state of the running sub containers in the app
    stateResults = deployed_contaner_status(uid)

    # debug
    # print(json.dumps(stateResults, indent=4, sort_keys=True))

    secCol = [row[2] for row in stateResults]
    # print(secCol)
    if any('STOPPED' in d for d in secCol):
        print('one or more of the contaners are stopped')
        if restartTry == 'true':
            print('trying to restart app')
            # try to restart useing the api
            restartupdate = d.startAppById(uid, {
                "allSelected": 'true',
                "note": "stopping"
            })
            # print(restartupdate)
            n = 1  # int counter
            t = 40  # max time the while loop should run
            while any('STOPPED' in d for d in secCol):
                if n <= t:
                    print(' app not started yet, this check way run ' + str(n) + ' times')
                    time.sleep(1)  # TODO add a more graceful checking gradient then just every one second
                    # update the app status
                    secCol = [row[2] for row in deployed_contaner_status(uid)]
                    # print(secCol) # debug
                else:
                    error = 'deployed app ' + uid + ' provisioning timed out'
                    return error
                n += 1  # counter var
            else:
                return 'The app is running'
        else:
            return 'The app is stopped'
    elif all('RUNNING' in d for d in secCol):
        return 'The app is running'
    else:
        error = stateResults
        return error


# print(json.dumps(d.restartAppById('2c918086598439c10159abbfdf2c3d11'), indent=4, sort_keys=True))
# print(json.dumps(contaner_status_check('2c9180865a969c44015a98564228015d', 'true'), indent=4, sort_keys=True))


def app_web_deploy(uid, contanerOwner, restartTry):
    # TODO add a if statement to make sure the user in authorised to deploy the blueprint uid
    # deploy based off of the uid
    deployedApp = deploy_blueprint(uid, contanerOwner)

    # check the new app stasis

    # make sure that a PROVISIONING was returned
    if deployedApp[0][1] == 'PROVISIONING':
        list = []
        # debug
        # print(deployedApp[0][0])
        # take a second for the remote server to catch up
        # time.sleep(2) # not needed
        i = 1  # int counter
        t = 40  # max time the while loop should run
        # if the app is stopped sleep for 1 sec then refresh the app's stasis
        while deployed_contaner_check(deployedApp[0][0], restartTry) != 'The app is running':
            # make sure the provisioning can fail gracefully
            if i <= t:
                print(deployedApp[0][0] + ' app not started, check run ' + str(i) + ' times')
                time.sleep(1)  # TODO add a more graceful checking gradient then just every one second
            else:
                error = 'deployed app ' + deployedApp[0][0] + ' provisioning timed out'
                return error
            i += 1  # counter var
        else:
            print(deployedApp[0][0] + ' app started')
            list_row = []
            list_row += [deployedApp[0][0], deployedApp[0][2], deployedApp[0][3], str(datetime.now())]
            list.append(list_row)
            # debug
            # print(list)
            return list
    else:
        error = 'app server deployment error, stuck in a' + deployedApp[0][1] + 'state'
        return error


# print(app_web_deploy('2c9180865ab60395015ab6c78d7c0317', 'admin', 'true'))

# while deployed_contaner_check('2c9180865a295e0a015a5dcbfd7e5e87', 'true') != 'The app is running':
#     time.sleep(1)
#     print('stoped')
# else:
#     print('runing')


# print(d.createBlueprint({
# "name": "amal-Nginx-test",
# "blueprintType": "DOCKER_COMPOSE",
# "version": "2.0",
# "yml": "traefik:"}));

# i = 2
# t = 10000
# while i <= t:
#     # if not process_acceptable_value(value):
#     #     # something went wrong, exit the loop; don't pass go, don't collect 200
#     #     break
#     print(i)
#     # i += 1
#     if i == 39:
#         print("something went wrong, exit the loop; don't pass go, don't collect 200")
#         break
#     # double the counting var
#     i **= 2
# else:
#     # value >= threshold; pass go, collect 200
#     # handle_threshold_reached()
#     print("fail")


# x = 10
# for i in range (x):
#     exp = int(2**(i/1.0))
#     print(exp)




import subprocess as sp


def ping(host):
    status, result = sp.getstatusoutput("ping -c1 -w2 " + str(host))
    if status == 0:
        list_row = []
        list_row += [str(host), 'UP', str(datetime.now())]
        return list_row
    else:
        list_row = []
        list_row += [str(host), 'DOWN', str(datetime.now())]
        return list_row


# print(ping('192.168.1.2'))

def app_hostname_build(uid):
    appInfo = d.getAppById(uid)
    list = []
    if appInfo['errors'] != 'false':
        for i in range(0, (len(appInfo['results']['containers']))):
            for j in range(0, (len(appInfo['results']['containers'][i]['envs']))):
                # print(appInfo['results']['containers'][i]['envs'][j]['prop'])
                if appInfo['results']['containers'][i]['envs'][j]['prop'] == 'external_hostname' and \
                        appInfo['results']['containers'][i]['envs'][j]['val'] == 'true':
                    list_row = []
                    appExpose = (appInfo['results']['containers'][i]['containerName'] + '.docker.nwdnw.tk')
                    list_row += [appExpose]
                    list.append(list_row)
        # fqdn = list + '.docker.nwdnw.tk'
        return list
    else:
        error = 'Error checking container FQDN status'
        return error


# print(app_hostname_build('2c9180865ab60395015ac5433f303e8e'))

# if list(map(bool, app_hostname_build('2c9180865ab60395015ac5433f303e8e'))).count(True) == 1:
#     # print(app_hostname_build('2c9180865ab60395015ac5433f303e8e'))
#     a = app_hostname_build('2c9180865ab60395015ac5433f303e8e')
#     str1 = ''.join([str(item) for sublist in a for item in sublist])
#     print(str1)
# else:
#     print('erro')

# print(json.dumps(app_hostname_build('2c9180865ab60395015ac5433f303e8e'), indent=4, sort_keys=True))
# print(ping(app_hostname_build('2c9180865ab60395015abce75aea180e')))

def remove_App(uid):
    removedApp = d.destroyAppById(uid, {'allSelected': 'true'});
    return removedApp


# print(remove_App('2c9180865ab60395015acb48639156e9'))

def app_build_custom_blurprint(name, data):
    new_bp=[]
    new_bp = d.createBlueprint({
        "name": name,
        "blueprintType": "DOCKER_COMPOSE",
        "version": "2.0",
        "composeVersion": "V2",
        "yml": data
    })

    # print(new_bp)

    list = []
    # Ensure variable is defined
    try:
        num = new_bp["messages"][0]["messageType"]
    except:
        num = None

    print(num)
    list = []
    # Test whether variable is defined to be None
    if num == "ERROR":
        return new_bp["messages"][0]
    else:
        # check to see if an error message was returned
        blueprint_name = new_bp['results']['name']
        blueprint_uid = new_bp['results']['id']
        blueprint_visibility = new_bp['results']['visibility']
        blueprint_yml = new_bp['results']['yml']
        list_row = []
        list_row += [blueprint_name, blueprint_uid, blueprint_visibility, blueprint_yml, str(datetime.now())]
        list.append(list_row)
        return list


new_bp_yaml = (
'version: \'2\'\n\nservices:\n  whoami:\n    image: emilevauge/whoami\n    environment:\n      - external_hostname=true\n    networks: \n      - traefik-net\n    labels:\n      - \"traefik.backend=whoami\"\n      - \"traefik.frontend.rule=Host:whoami.docker.localhost\"\n')

print(json.dumps(app_build_custom_blurprint("123t45d6fcdd0254ce5vfd45fst-bp", new_bp_yaml), indent=4, sort_keys=True))


# print(json.dumps(app_build_custom_blurprint(), indent=4, sort_keys=True))

def app_deleat_custom_blueprint(uid):
    result = d.deleteBlueprintById(uid)
    return result

# print(json.dumps(app_deleat_custom_blueprint('2c9180865ae25e7b015b9ce0503f2779'), indent=4, sort_keys=True))
