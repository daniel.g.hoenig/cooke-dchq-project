from __future__ import absolute_import, unicode_literals
from django import forms
from django.forms import ModelChoiceField, ModelForm, ChoiceField

from app.models import Blueprint, userApps, userCustomBlueprintTemplates


class app_create_form_int(forms.ModelForm):
    templateName = forms.CharField(widget = forms.TextInput(attrs={'readonly':'readonly'}))
    class Meta:
        model = userApps
        fields = ['AppName',
                  'appFqdn',
                  ]
        widgets = {
            "AppName": forms.TextInput(attrs={'pattern':".{4,}", 'required title':"4 characters minimum"}),
            "appFqdn": forms.TextInput(attrs={'pattern':".{4,}", 'required title':"4 characters minimum"})
        }


class app_create_form(forms.ModelForm):
    # build the bp template drop down

    class Meta:
        model = userApps
        fields = ['AppName',
                  'appFqdn']
        widgets = {
            # "password": forms.PasswordInput(),
            "AppName": forms.TextInput(attrs={'pattern':".{4,}", 'required title':"4 characters minimum"}),
            "appFqdn": forms.TextInput(attrs={'pattern':".{4,}", 'required title':"4 characters minimum"})
        }

    templateName = forms.ModelChoiceField(queryset=userCustomBlueprintTemplates.objects.filter(templateVisibility__iexact='ENABLED'),to_field_name="templateName", required=True)

    # def __init__(self, *args, **kwargs):
    #     super(app_create_form, self).__init__(*args, **kwargs)
    #     # print(kwargs['initial']['templateName'])
    #     if 'initial' in kwargs:
    #         self.fields['templateName'].empty_label = None
    #         self.fields['templateName'].disabled = True
    #         self.fields['templateName'].required = False
    #         self.fields['templateName'].queryset = userCustomBlueprintTemplates.objects.filter(templateName__iexact='whoamiv1')
    #         self.initial['templateName'] = kwargs['initial']['templateName']
    #
    #         # self.fields['templateName'].widget.attrs['readonly'] = 'readonly'
    #     else:
    #         self.fields['templateName'].disabled = False
    #         self.fields['templateName'].empty_label = None
    #         # self.fields['templateName'].required = True
