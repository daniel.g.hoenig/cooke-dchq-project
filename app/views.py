# views.py
import json
from datetime import datetime

from celery.result import AsyncResult
from django.contrib.auth.decorators import login_required
from django.contrib.auth.mixins import LoginRequiredMixin
from django.core.urlresolvers import reverse_lazy
from django.http import Http404
from django.http import JsonResponse
from django.shortcuts import HttpResponse, get_object_or_404
from django.template.loader import render_to_string
from django.views.generic import DeleteView
from django.views.generic import ListView
from rest_framework import status
from rest_framework.decorators import api_view
from rest_framework.parsers import JSONParser
from rest_framework.response import Response

from .forms import app_create_form, app_create_form_int
from .models import userApps
from .serializers import userAppSerializer
from .tasks import app_web_deploy, app_web_remove_App


# ----------------------------------------------------------------------------------------

# Create your views here.


def poll_state(request):
    """ A view to report the progress to the user """
    data = 'Fail'
    if request.is_ajax():
        if 'task_id' in request.POST.keys() and request.POST['task_id']:
            task_id = request.POST['task_id']
            task = AsyncResult(task_id)
            # data = task.result or task.state

            if task.state == 'PENDING':
                data = {
                    'state': task.state,
                    'current': 0,
                    'total': 1,
                    'status': 'Pending...'
                }
            elif task.state != 'FAILURE':
                data = {
                    'state': task.state,
                    'current': task.info.get('current', 0),
                    'total': task.info.get('total', 1),
                    'status': task.info.get('status', ''),
                    'appUrl': task.info.get('fqdn', '')
                }
                if 'result' in task.info:
                    data['result'] = task.info['result']
            else:
                # something went wrong in the background job
                data = {
                    'state': task.state,
                    'current': 1,
                    'total': 1,
                    'status': str(task.info),  # this is the exception raised
                }

        else:
            data = 'No task_id in the request'

    else:
        data = 'This is not an ajax request'

    json_data = json.dumps(data)
    return HttpResponse(json_data, content_type='application/json')


# @login_required
# def index(request):
#     if 'job' in request.GET:
#         job_id = request.GET['job']
#         job = AsyncResult(job_id)
#         data = job.result or job.state
#         context = {
#             'data': data,
#             'task_id': job_id,
#         }
#         return render(request, "show_t.html", context)
#
#     elif request.method == 'POST':
#         # create a form instance and populate it with data from the request:
#         form = my_Form(request.POST)
#         # check whether it's valid:
#         if form.is_valid():
#             # fs = form.cleaned_data #debug
#             fs = form.cleaned_data['name']
#             # print(fs) # debug
#             filtered_blueprint_list = Blueprint.objects.filter(name__iexact=fs).as_list()
#             print(filtered_blueprint_list)
#             # form.save()
#             # print('it works')
#
#             # redirect to a new URL:
#             user = request.user
#             # print(user)
#             job = app_web_deploy.delay(filtered_blueprint_list[0][1], str(user), 'true')
#             # job = long_task.delay()
#             return HttpResponseRedirect(reverse('index') + '?job=' + job.id)
#
#         else:
#             # if error goto the index page
#             return HttpResponseRedirect('/index/')
#
#     else:
#         form = my_Form()
#         context = {
#             'form': form,
#         }
#         return render(request, "post_form.html", context)


# class dash(ListView):
#     model = userApps
#     template_name = 'dash.html'
#
#     def get_queryset(self):
#         return userApps.objects.filter(owner__exact='admin')

class DashboardPage(LoginRequiredMixin, ListView):
    model = userApps
    # template_name = '../../app/templates/app_list.html'
    # template_name = 'app/app_list.html'
    def get_queryset(self):
        queryset = super(DashboardPage, self).get_queryset()
        user = self.request.user
        queryset = userApps.objects.filter(owner__exact=user)
        return queryset


@login_required
def app_create(request, pk):
    data = dict()
    if request.method == 'POST':
        form = app_create_form(request.POST)
        if form.is_valid():
            obj = form.save(commit=False)
            data['form_is_valid'] = True  # send a notification to the apps js

            fs = form.cleaned_data['templateName']
            print(form) # debug

            # try:
            job = app_web_deploy.delay(str(fs), str(request.user), 'true', str(form.cleaned_data['AppName']),
                                       str(form.cleaned_data['appFqdn']))

            print(job)

            obj.appFqdn = '...loading...'
            obj.lastMod = datetime.now()
            obj.appStatus = 'PROVISIONING'
            obj.owner = request.user
            obj.save()

            # return the refresh the app table via the js
            user = request.user
            apps = userApps.objects.filter(owner__exact=user)  # make sure to only return owned apps.
            data['html_book_list'] = render_to_string('app/includes/partial_app_list.html', {
                'userapps_list': apps
            })
            # except:
            #     data['error'] = True  # send a job error notification
        else:
            data['form_is_valid'] = False

    # check for a url parameter and if so pass it as a initial form value
    if pk:
        form = app_create_form_int(initial={'templateName': pk})
        context = {'form': form}
        data['html_form'] = render_to_string('app/includes/partial_app_create.html',
                                             context,
                                             request=request
                                             )
        return JsonResponse(data)
    else:
        form = app_create_form()
        context = {'form': form}
        data['html_form'] = render_to_string('app/includes/partial_app_create.html',
                                             context,
                                             request=request
                                             )
        return JsonResponse(data)


def app_delete(request, pk):
    user_app = get_object_or_404(userApps, pk=pk)
    data = dict()
    if request.method == 'POST':
        # get the logged in user
        user = request.user
        # check that the app to delete is owned by the logged in user
        if userApps.objects.filter(owner__exact=user):
            # calling the dchq delete function
            appUid = userApps.objects.get(id=pk).APPuid  # use the app db ID value to get the app uid
            bpUid = userApps.objects.get(id=pk).blueprintUid  # get the build bp id from the db
            job = app_web_remove_App.delay(appUid, bpUid)
            print(job)
        else:
            raise Http404("App you are looking for doesn't exist")

        user_app.delete()  # delete the db object
        data['form_is_valid'] = True  # This is just to play along with the existing code
        #  load the app list with the new objects
        apps = userApps.objects.filter(owner__exact=user)
        data['html_book_list'] = render_to_string('app/includes/partial_app_list.html', {
            'userapps_list': apps
        })
    else:
        context = {'userApps': user_app}  # use the info from the get_object_or_404
        data['html_form'] = render_to_string('app/includes/partial_app_delete.html',
                                             context,
                                             request=request,
                                             )
    return JsonResponse(data)


class AppDelete(DeleteView):
    model = userApps
    success_url = reverse_lazy('dashboard')  # This is where this view will
    # redirect the user
    template_name = 'delete_app.html'
    context_object_name = 'delete_app'

    # def get_object(self, queryset=None):
    #     if queryset is None:
    #         queryset = self.get_queryset()
    #
    #     client = self.kwargs['pk']
    #
    #     queryset = userApps.objects.filter(client_id=client)
    #
    #     if not queryset:
    #         raise Http404
    #
    #     context = {'client_id':client, 'report_id':report}
    #     return context

    def delete(self, request, *args, **kwargs):
        # get the logged in user
        user = self.request.user
        # check that the app to delete is owned by the logged in user
        if userApps.objects.filter(owner__exact=user):
            # calling the dchq delete function
            client = self.kwargs['pk']  # get the pk value being passed via the url
            appUid = userApps.objects.get(id=client).APPuid  # use the app db ID value to get the app uid
            bpUid = userApps.objects.get(id=client).blueprintUid
            job = app_web_remove_App.delay(appUid, bpUid)
            print(job)
            return super(AppDelete, self).delete(
                request, *args, **kwargs)
        else:
            raise Http404("App you are looking for doesn't exist")


@login_required
@api_view(['GET', 'POST'])
def app_list(request):
    """
    List all code snippets, or create a new snippet.
    """
    if request.method == 'GET':
        snippets = userApps.objects.all()
        serializer = userAppSerializer(snippets, many=True)
        return Response(serializer.data)

    elif request.method == 'POST':
        data = JSONParser().parse(request)
        serializer = userAppSerializer(data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data, status=status.HTTP_201_CREATED)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)


@login_required
@api_view(['GET', 'PUT', 'DELETE'])
def app_detail(request, pk):
    """
    Retrieve, update or delete a code snippet.
    """
    try:
        # make sure that a user can only poll apps they own
        # get the logged in user
        user = request.user.username
        # check that the app to delete is owned by the logged in user
        if userApps.objects.filter(owner__exact=user):
            snippet = userApps.objects.get(id=pk)
    except userApps.DoesNotExist:
        return Response(status=status.HTTP_404_NOT_FOUND)

    if request.method == 'GET':
        serializer = userAppSerializer(snippet)
        return Response(serializer.data)

    elif request.method == 'PUT':
        data = JSONParser().parse(request)
        serializer = userAppSerializer(snippet, data=data)
        if serializer.is_valid():
            serializer.save()
            return Response(serializer.data)
        return Response(serializer.errors, status=status.HTTP_400_BAD_REQUEST)

    elif request.method == 'DELETE':
        # get the logged in user
        user = request.user
        # check that the app to delete is owned by the logged in user
        if userApps.objects.filter(owner__exact=user):
            # calling the dchq delete function
            appUid = userApps.objects.get(id=pk).APPuid  # use the app db ID value to get the app uid
            bpUid = userApps.objects.get(id=pk).blueprintUid  # get the build bp id from the db
            job = app_web_remove_App.delay(appUid, bpUid)  # run the app remove task
            print(job)
        else:
            raise Http404("App you are looking for doesn't exist")

        snippet.delete()
        return Response(status=status.HTTP_204_NO_CONTENT)

@login_required
@api_view(['GET'])
def check_app_detail(request):
    data = dict()
    if 'appname' in request.GET and request.GET['appname']:
        # filter for /app/check_app_detail/?appname=value
        appname = request.GET['appname']
        print(appname)
        if userApps.objects.filter(AppName__exact=appname):
            print("There is at least one Entry with the headline Test")
            # return HttpResponse('/static/images/check_username_bad.png', content_type='text/plain')
            data['name_exists'] = True
            data['status_image'] = '/static/images/check_username_bad.png'
        else:
            # return HttpResponse('/static/images/check_username_ok.png', content_type='text/plain')
            data['name_exists'] = False
            data['status_image'] = '/static/images/check_username_ok.png'
        return JsonResponse(data)
    if 'appfqdn' in request.GET and request.GET['appfqdn']:
        # filter for /app/check_app_detail/?appfqdn=value
        appfqdn = request.GET['appfqdn']
        print(appfqdn)
        if userApps.objects.filter(appFqdn__icontains=appfqdn):
            print("There is at least one Entry with the headline Test")
            # return HttpResponse('/static/images/check_username_bad.png', content_type='text/plain')
            data['name_exists'] = True
            data['status_image'] = '/static/images/check_username_bad.png'
        else:
            # return HttpResponse('/static/images/check_username_ok.png', content_type='text/plain')
            data['name_exists'] = False
            data['status_image'] = '/static/images/check_username_ok.png'
        return JsonResponse(data)
    else:
        return Response(status=status.HTTP_404_NOT_FOUND)
