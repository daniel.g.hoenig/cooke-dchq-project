from django.contrib import admin

# Register your models here.
from .models import dchqConnection, Blueprint, userCustomBlueprintTemplates, userApps

# Register your models here.
admin.site.register(dchqConnection)
admin.site.register(Blueprint)
admin.site.register(userCustomBlueprintTemplates)
admin.site.register(userApps)
