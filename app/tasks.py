# tasks.py
# from __future__ import absolute_import, unicode_literals
import random
import time
from datetime import datetime

from celery import shared_task, current_task

from .dchq_deploy_tasks import deploy_blueprint, deployed_contaner_check, ping, remove_App, app_build_custom_blurprint, \
    app_deleat_custom_blueprint
# from numpy import random
# from scipy.fftpack import fft
from .models import userApps, userCustomBlueprintTemplates


# @shared_task
# def fft_random(n):
#     """
#     Brainless number crunching just to have a substantial task:
#     """
#     for i in range(n):
#         x = random.normal(0, 0.1, 2000)
#         y = fft(x)
#         if (i % 30 == 0):
#             process_percent = int(100 * float(i) / float(n))
#             current_task.update_state(state='PROGRESS',
#                                       meta={'process_percent': process_percent})
#     return random.random()
#
#
# @shared_task
# def long_task():
#     """Background task that runs a long function with progress reports."""
#     verb = ['Starting up', 'Booting', 'Repairing', 'Loading', 'Checking']
#     adjective = ['master', 'radiant', 'silent', 'harmonic', 'fast']
#     noun = ['solar array', 'particle reshaper', 'cosmic ray', 'orbiter', 'bit']
#     message = ''
#     total = random.randint(10, 50)
#     for i in range(total):
#         if not message or random.random() < 0.25:
#             message = '{0} {1} {2}...'.format(random.choice(verb),
#                                               random.choice(adjective),
#                                               random.choice(noun))
#             current_task.update_state(state='PROGRESS',
#                                       meta={'current': i, 'total': total,
#                                             'status': message})
#         time.sleep(1)
#     return {'current': 100, 'total': 100, 'status': 'Task completed!',
#             'result': 42}


@shared_task
def app_web_deploy(templateName, contanerOwner, restartTry, AppName, appFqdn):

    # build the users custom blueprint based off the passed form data #TODO need to add something to check if a user is authorized to use a template.

    # get the blueprint YAML from the db
    # print(templateName)
    template_yaml = userCustomBlueprintTemplates.objects.get(templateName__iexact=templateName).serializable_value('templateYAML') % (appFqdn, appFqdn) # get the tempalte YAML from the db and add the fqdn prefix's
    '''
    Old template YAML with the escape characters hard codded into the string.
        template_yaml = ('version: \'2\'\n\nservices:\n  whoami:\n    image: emilevauge/whoami\n    environment:\n      - external_hostname=true\n    networks: \n      - traefik-net\n    labels:\n      - \"traefik.backend=whoami\"\n      - \"traefik.frontend.rule=Host:whoami.docker.localhost\"\n')
    
        template_yaml = 'version: \'2\'\n\nservices:\n  whoami:\n    image: emilevauge/whoami\n    environment:\n      - external_hostname=true\n    networks: \n      - traefik-net\n    labels:\n      - \"traefik.backend=%s\"\n      - \"traefik.frontend.rule=Host:%s.docker.nwdnw.tk\"\n' % (appFqdn, appFqdn)
    '''
    # create the bp on the remote server
    new_bp = app_build_custom_blurprint(AppName, template_yaml)
    print(new_bp)
    # get the uid from the new bp
    bp_uid = new_bp[0][1]

    # deploy based off of the uid  # TODO add a if statement to make sure the user in authorised to deploy the blueprint uid
    deployedApp = deploy_blueprint(bp_uid, contanerOwner)

    # check the new app stasis

    # make sure that a PROVISIONING was returned
    if deployedApp[0][1] == 'PROVISIONING':

        try:
            # saving the int PROVISIONING state to the db

            obj = userApps.objects.get(AppName=AppName)
            # obj = userApps.objects.get(APPuid=deployedApp[0][0])
            # TODO add here any functions to build a CNAME linking from the local fqdn to a external fqdn.
            # Update all the headlines belonging to this Blog.
            obj.APPuid = deployedApp[0][0]
            obj.blueprintUid = bp_uid
            obj.lastMod = datetime.now()
            obj.appStatus = 'PROVISIONING'
            obj.blueprintYAML = template_yaml
            obj.save()

            print('PROVISIONING app status created in the db')
        except userApps.DoesNotExist:
            # make a db intrey if none exist  #  TODO need to fix
            obj = userApps(APPuid=deployedApp[0][0], owner=contanerOwner, blueprintUid=bp_uid, buildTime=datetime.now(), lastMod=datetime.now(), appStatus='PROVISIONING', AppName=AppName)  # build the string to wright to the db
            obj.save()  # save or update the db

        list = []
        # debug
        # print(deployedApp[0][0])
        # take a second for the remote server to catch up
        # time.sleep(2) # not needed
        i = 1  # int counter
        t = 100  # max time the while loop should run
        # if the app is stopped sleep for 1 sec then refresh the app's stasis
        while deployed_contaner_check(deployedApp[0][0], restartTry) != 'The app is running':
            # make sure the provisioning can fail gracefully
            if i <= t:
                print(deployedApp[0][0] + ' app not started, check run ' + str(i) + ' times')
                # ------
                verb = ['Starting up', 'Booting', 'Repairing', 'Loading', 'Checking']
                adjective = ['master', 'radiant', 'silent', 'harmonic', 'fast']
                noun = ['solar array', 'particle reshaper', 'cosmic ray', 'orbiter', 'bit']
                message = '{0} {1} {2}...'.format(random.choice(verb),
                                                  random.choice(adjective),
                                                  random.choice(noun))
                current_task.update_state(state='PROGRESS',
                                          meta={'current': i, 'total': str(i),
                                                'status': message})

                # --------
                time.sleep(1)  # TODO add a more graceful checking gradient then just every one second
            else:
                error = 'deployed app ' + deployedApp[0][0] + ' provisioning timed out'
                return error
            i += 1  # counter var
        else:
            # app is deployed, now build the hostname
            # app_hostname = app_hostname_build(deployedApp[0][0]) # deployedApp[0][0] will return the new app's uid
            # print(app_hostname)
            # str1 = ''.join([str(item) for sublist in app_hostname for item in sublist])
            fullAppFqdn = appFqdn+'.docker.nwdnw.tk' # TODO change the full fqdn from hard coded into some db value or global var
            print(fullAppFqdn)
            if fullAppFqdn is not None: # check if only one contaner is marked to be exposed.
                # print(deployedApp)
                # print(app_hostname)
                print(deployedApp[0][0] + ' app started')
                # print(ping(fullAppFqdn)) #debug
                time.sleep(5) # temp pause for 5 sec's, to fix the down issue
                print(ping(fullAppFqdn))  # debug
                if ping(fullAppFqdn)[1] == 'UP':
                    list_row = []
                    list_row += [deployedApp[0][0], deployedApp[0][2], deployedApp[0][3], str(fullAppFqdn), str(datetime.now())]
                    list.append(list_row)
                    # debug
                    # print(list)
                    # return list

                    try:
                        obj = userApps.objects.get(APPuid=deployedApp[0][0])
                        # TODO add here any functions to build a CNAME linking from the local fqdn to a external fqdn.
                        # Update all the headlines belonging to this Blog.
                        obj.lastMod = datetime.now()
                        obj.appFqdn = fullAppFqdn
                        obj.appLocalFqdn = fullAppFqdn
                        obj.appStatus = 'RUNNING'
                        obj.save()
                        print('db updated with apps fqdn and RUNNING status')
                    except userApps.DoesNotExist:
                        # obj = userApps(APPuid=deployedApp[0][0], owner=contanerOwner, blueprintUid=uid, buildTime=datetime.now(), lastMod=datetime.now(), appFqdn=str1, appLocalFqdn=str1) #build the string to wright to the db
                        # obj.save() # save or update the db
                        # print('deployed app status saved to db')
                        print('ERROR, there should already be a db entry created for this app')


                    return {'current': 100, 'total': 100, 'status': 'Task completed!',
                            'result': 42, 'list': list, 'fqdn': fullAppFqdn}
                else:
                    error = 'new app ' + deployedApp[0][0] + ' fqdn ' + fullAppFqdn + ' is DOWN'
                    try:
                        obj = userApps.objects.get(APPuid=deployedApp[0][0])
                        obj.lastMod = datetime.now()
                        obj.appFqdn = ''
                        obj.appLocalFqdn = ''
                        obj.appStatus = 'ERROR'
                        obj.save()
                        print(error)
                    except userApps.DoesNotExist:
                        error = 'ERROR, there should already be a db entry created for this app'
                        print(error)
                        return error
            else:
                error = 'ERROR: more then one containers with a exposed fqdn'
                try:
                    obj = userApps.objects.get(APPuid=deployedApp[0][0])
                    obj.lastMod = datetime.now()
                    obj.appStatus = 'ERROR'
                    obj.save()
                    print(error)
                except userApps.DoesNotExist:
                    error = 'ERROR, there should already be a db entry created for this app'
                    print(error)
                    return error

    else:
        error = 'app server deployment error, stuck in a' + deployedApp[0][1] + 'state'
        # make sure that the db is updated
        try:
            obj = userApps.objects.get(APPuid=deployedApp[0][0])
            obj.lastMod = datetime.now()
            obj.appStatus = 'ERROR'
            obj.save()
            print(error)
        except userApps.DoesNotExist:
            error = 'ERROR, there should already be a db entry created for this app'
            print(error)
            return error

        # debug usage
        # print(app_web_deploy('2c91808656b597ff0156b5d9f1ed0042', 'admin', 'true'))

@shared_task
def app_web_remove_App(uid, bp_uid):
    # TODO need to add appropriate exception handling
    list = []
    # remove the app from the server
    removedAppOut = remove_App(uid)
    # remove the bp from the server
    removeAppBP = app_deleat_custom_blueprint(bp_uid)

    list_row = []
    list_row += [removedAppOut, removeAppBP]
    list.append(list_row)
    # return the fact
    print('app remove task succeeding')
    return list
