# dchq_deploy_tasks.py
# from __future__ import absolute_import, unicode_literals

# --------------------------------------------------------------------
# needed to fix the module import error
import json
import sys

# sys.path.append('/home/daniel/Projects/DCHQ-Python-SDK/dchq') # not needed, beaks all other imports.
# from kombu.utils import json



# sys.path.insert(1, '/home/daniel/Projects/DCHQ-Python-SDK/dchq')
# full path the the dir with the dchq library
# --------------------------------------------------------------------


from datetime import datetime
import time

from app.dchq.dchq import Dchq
from .models import dchqConnection
import subprocess as sp

# trying to make sure that the migrations will work
try:
    # get the endpoint details from the django db
    # x = dchqConnection.objects.get()  # (old) dchqConnection.objects.values_list('username')
    # dchq connection string

    # d = Dchq({'username': x.username, 'password': x.password, 'endpoint': x.hostEndpoint})

    d = Dchq({'username': 'admin@dchq.io', 'password': 'admin123', 'endpoint': '192.168.1.169'}) # old hardcoded  # connection string
except:
    print('ERROR: no dchq connection set up')


def get_all_blueprint():
    data = d.getBlueprints('1',
                           '100')  # TODO: replace in the future with some logic for the page size. The "totalElements": 77, "totalPages": 1 elealment should do the trick.
    list = []
    for i in range(0, (len(data['results']))):
        list_row = []
        for row in range(0, (len(data['results']))):
            bpId = (data['results'][i]['id'])
            bpNmae = (data['results'][i]['name'])
        list_row += [bpNmae, bpId]
        list.append(list_row)

    return list


# print(get_all_blueprint())
# print(json.dumps(d.getBlueprints('1', '100'), indent=4, sort_keys=True));

def test(*blueprintType):
    if 'dc' in blueprintType:
        blueprintType = 'DOCKER_COMPOSE'
    if 'vm' in blueprintType:
        blueprintType = 'VM_COMPOSE'
    if not blueprintType:
        blueprintType = 'all'
    data = d.getBlueprints('1', '100')
    list = []
    for i in range(0, (len(data['results']))):
        if (blueprintType in data['results'][i]['blueprintType']) and (blueprintType != 'all'):
            list_row = []
            for row in range(0, (len(data['results']))):
                bpId = (data['results'][i]['id'])
                bpNmae = (data['results'][i]['name'])
                bgTag = (data['results'][i]['tags'])
            list_row += [bpNmae, bpId, blueprintType, bgTag]
            list.append(list_row)
        if blueprintType == 'all':
            list_row = []
            for row in range(0, (len(data['results']))):
                bpId = (data['results'][i]['id'])
                bpNmae = (data['results'][i]['name'])
                bpType = (data['results'][i]['blueprintType'])
                bgTag = (data['results'][i]['tags'])
            list_row += [bpNmae, bpId, bpType, bgTag]
            list.append(list_row)

    return list


# print(json.dumps(test('dc'), indent=4, sort_keys=True)); # 'VM_COMPOSE'(vm) or 'DOCKER_COMPOSE'(dc) or blank for all types

def deploy_contaner_blueprint(blueprintUID):
    data = d.deployAppById(blueprintUID);
    return data


# print(json.dumps(deploy_contaner_blueprint('2c91808656b597ff0156b5d9f1ed0042'), indent=4, sort_keys=True));  #make a new contaner

# print(json.dumps(d.getAppById('2c918086591e503801597842adb93668'), indent=4, sort_keys=True)) #get the new contaners


def deploy_blueprint(uid, contanerOwner):
    # call the deploy app function
    postResult = d.deployAppById(uid)
    # create an empty dir
    list = []
    # Ensure variable is defined
    try:
        num = postResult["messages"][0]["messageType"]
    except:
        num = None

    # print(num)
    list = []
    # Test whether variable is defined to be None
    if num == "ERROR":
        return postResult["messages"][0]
    else:
        for i in range(0, (len(postResult['results']))):
            list_row = []
            # contanerOwner = 'admin'
            contanterCreateTime = str(datetime.now())
            for row in range(0, (len(postResult['results']))):
                bpId = (postResult['results']['id'])
                bpProvisionState = (postResult['results']['provisionState'])
            list_row += [bpId, bpProvisionState, contanerOwner, contanterCreateTime]
            list.append(list_row)

            return list


# print(json.dumps(deploy_blueprint('2c91808656b597ff0156b5d9f1ed0042'), indent=4, sort_keys=True))
# print(deploy_blueprint('2c91808656b597ff0156b5d9f1ed0042', 'admin'))


def deployed_contaner_status(uid):
    #  call the update dchq function
    updateResult = d.getAppById(uid)
    # create an empty dir
    list = []
    # Ensure variable is defined
    try:
        num = updateResult["messages"][0]["messageType"]
    except:
        num = None

    print(num)
    list = []
    # Test whether variable is defined to be None
    if num == "ERROR":
        return updateResult["messages"][0]
    else:

        # check to make sure that the app is running, got to check all the states to pervent getting an error
        if updateResult['results']['provisionState'] == 'RUNNING':
            # if the provisionState is running check to see if all the containers are running
            list = []
            for i in range(0, (len(updateResult['results']['containers']))):
                # if everything in the container is running, return that fact
                if updateResult['results']['containers'][i]['containerExpectedStatus'] == 'RUNNING' and \
                        updateResult['results']['containers'][i]['containerStatus'] == 'RUNNING':
                    list_row = []
                    case = updateResult['results']['containers'][i]['containerId']
                    containerName = updateResult['results']['containers'][i]['containerName']
                    containerStatus = updateResult['results']['containers'][i]['containerStatus']
                    list_row += [case, containerName, containerStatus]
                    list.append(list_row)
                # if everything in the container is stoped, return that fact
                if updateResult['results']['containers'][i]['containerExpectedStatus'] == 'RUNNING' and \
                        updateResult['results']['containers'][i]['containerStatus'] == 'STOPPED':
                    list_row = []
                    case = updateResult['results']['containers'][i]['containerId']
                    containerName = updateResult['results']['containers'][i]['containerName']
                    containerStatus = updateResult['results']['containers'][i]['containerStatus']
                    list_row += [case, containerName, containerStatus]
                    list.append(list_row)
            return list
        if updateResult['results']['provisionState'] == 'STOPPED':
            # if the provisionState is running check to see if all the containers are running
            list = []
            for i in range(0, (len(updateResult['results']['containers']))):
                # if everything in the container is running, return that fact
                if updateResult['results']['containers'][i]['containerExpectedStatus'] == 'RUNNING' and \
                        updateResult['results']['containers'][i]['containerStatus'] == 'RUNNING':
                    list_row = []
                    case = updateResult['results']['containers'][i]['containerId']
                    containerName = updateResult['results']['containers'][i]['containerName']
                    containerStatus = updateResult['results']['containers'][i]['containerStatus']
                    list_row += [case, containerName, containerStatus]
                    list.append(list_row)
                # if everything in the container is stoped, return that fact
                if updateResult['results']['containers'][i]['containerExpectedStatus'] == 'RUNNING' and \
                        updateResult['results']['containers'][i]['containerStatus'] == 'STOPPED':
                    list_row = []
                    case = updateResult['results']['containers'][i]['containerId']
                    containerName = updateResult['results']['containers'][i]['containerName']
                    containerStatus = updateResult['results']['containers'][i]['containerStatus']
                    list_row += [case, containerName, containerStatus]
                    list.append(list_row)
            return list
        if updateResult['results']['provisionState'] == 'PROVISIONING':
            # if the provisionState is running check to see if all the containers are running
            list = []
            for i in range(0, (len(updateResult['results']['containers']))):
                # if everything in the container is running, return that fact
                if updateResult['results']['containers'][i]['containerExpectedStatus'] == 'RUNNING' and \
                        updateResult['results']['containers'][i]['containerStatus'] == 'RUNNING':
                    list_row = []
                    case = updateResult['results']['containers'][i]['containerId']
                    containerName = updateResult['results']['containers'][i]['containerName']
                    containerStatus = updateResult['results']['containers'][i]['containerStatus']
                    list_row += [case, containerName, containerStatus]
                    list.append(list_row)
                # if everything in the container is stoped, return that fact
                if updateResult['results']['containers'][i]['containerExpectedStatus'] == 'RUNNING' and \
                        updateResult['results']['containers'][i]['containerStatus'] == 'STOPPED':
                    list_row = []
                    case = updateResult['results']['containers'][i]['containerId']
                    containerName = updateResult['results']['containers'][i]['containerName']
                    containerStatus = updateResult['results']['containers'][i]['containerStatus']
                    list_row += [case, containerName, containerStatus]
                    list.append(list_row)
            return list


# print(json.dumps(deployed_contaner_status('2c9180865a295e0a015a5dcf562f5eb3'), indent=4, sort_keys=True))
# print(deployed_contaner_status('2c9180865a295e0a015a5dcf562f5eb3'))


def deployed_contaner_check(uid, restartTry):
    # debug
    # print(uid)

    # get the state of the running sub containers in the app
    stateResults = deployed_contaner_status(uid)

    # debug
    # print(json.dumps(stateResults, indent=4, sort_keys=True))

    if stateResults != 'app stuck in PROVISIONING_FAILED':  # if statement added to help pervent (len(stateResults))) error. TODO net to implement a better system
        for i in range(0, (len(stateResults))):
            if stateResults[i][2] == 'STOPPED':
                return 'The app is stopped'
            if stateResults[i][2] == 'RUNNING':
                return 'The app is running'
                # return stateResults
            if stateResults[i][2] == 'RUNNING' and stateResults[i][2] == 'STOPPED':
                return 'The app is stopped'
                # return stateResults
    else:
        return 'app stuck in PROVISIONING_FAILED'


# print(json.dumps(d.restartAppById('2c918086598439c10159abbfdf2c3d11'), indent=4, sort_keys=True))
# print(json.dumps(deployed_contaner_check('2c918086598439c10159ae55cd2d424b', 'true'), indent=4, sort_keys=True))


def app_web_deploy(uid, contanerOwner, restartTry):
    # TODO add a if statement to make sure the user in authorised to deploy the blueprint uid
    # deploy based off of the uid
    deployedApp = deploy_blueprint(uid, contanerOwner)

    # check the new app stasis

    # make sure that a PROVISIONING was returned
    if deployedApp[0][1] == 'PROVISIONING':
        list = []
        # debug
        # print(deployedApp[0][0])
        # take a second for the remote server to catch up
        # time.sleep(2) # not needed
        i = 1  # int counter
        t = 40  # max time the while loop should run
        # if the app is stopped sleep for 1 sec then refresh the app's stasis
        while deployed_contaner_check(deployedApp[0][0], restartTry) != 'The app is running':
            # make sure the provisioning can fail gracefully
            if i <= t:
                print(deployedApp[0][0] + ' app not started, check run ' + str(i) + ' times')
                time.sleep(1)  # TODO add a more graceful checking gradient then just every one second
            else:
                error = 'deployed app ' + deployedApp[0][0] + ' provisioning timed out'
                return error
            i += 1  # counter var
        else:
            print(deployedApp[0][0] + ' app started')
            list_row = []
            list_row += [deployedApp[0][0], deployedApp[0][2], deployedApp[0][3], str(datetime.now())]
            list.append(list_row)
            # debug
            # print(list)
            return list
    else:
        error = 'app server deployment error, stuck in a' + deployedApp[0][1] + 'state'
        return error


# print(app_web_deploy('2c91808656b597ff0156b5d9f1ed0042', 'admin', 'true'))

def ping(host):
    status, result = sp.getstatusoutput("ping -c1 -w2 " + str(host))
    if status == 0:
        list_row = []
        list_row += [str(host), 'UP', str(datetime.now())]
        return list_row
    else:
        list_row = []
        list_row += [str(host), 'DOWN', str(datetime.now())]
        return list_row


# print(ping('192.168.1.2'))


def app_hostname_build(uid):
    appInfo = d.getAppById(uid)
    list = []
    if appInfo['errors'] != 'false':
        for i in range(0, (len(appInfo['results']['containers']))):
            for j in range(0, (len(appInfo['results']['containers'][i]['envs']))):
                # print(appInfo['results']['containers'][i]['envs'][j]['prop'])
                if appInfo['results']['containers'][i]['envs'][j]['prop'] == 'external_hostname' and \
                        appInfo['results']['containers'][i]['envs'][j]['val'] == 'true':
                    list_row = []
                    appExpose = (appInfo['results']['containers'][i]['containerName'] + '.docker.nwdnw.tk')
                    list_row += [appExpose]
                    list.append(list_row)
        # fqdn = list + '.docker.nwdnw.tk'
        return list
    else:
        error = 'Error checking container FQDN status'
        return error


# print(app_hostname_build('2c9180865a969c44015a98564228015d'))
# print(ping(app_hostname_build('2c9180865a969c44015a98564228015d')))

def remove_App(uid):
    removedApp = d.destroyAppById(uid, {'allSelected': 'true'});
    return removedApp


# print(remove_App('2c9180865ab60395015acb48639156e9'))


def app_build_custom_blurprint(name, data):
    new_bp = []
    new_bp = d.createBlueprint({
        "name": name,
        "blueprintType": "DOCKER_COMPOSE",
        "version": "2.0",
        "composeVersion": "V2",
        "yml": data
    })

    # print(new_bp)

    list = []
    # Ensure variable is defined
    try:
        num = new_bp["messages"][0]["messageType"]
    except:
        num = None

    print(num)
    list = []
    # Test whether variable is defined to be None
    if num == "ERROR":
        return new_bp["messages"][0]
    else:
        blueprint_name = new_bp['results']['name']
        blueprint_uid = new_bp['results']['id']
        blueprint_visibility = new_bp['results']['visibility']
        blueprint_yml = new_bp['results']['yml']
        list_row = []
        list_row += [blueprint_name, blueprint_uid, blueprint_visibility, blueprint_yml, str(datetime.now())]
        list.append(list_row)
        return list


# new_bp_yaml = "version: \'2\'\n\nservices:\n  whoami:\n    image: emilevauge/whoami\n    environment:\n      - external_hostname=true\n    networks: \n      - traefik-net\n    labels:\n      - \"traefik.backend=whoami\"\n      - \"traefik.frontend.rule=Host:whoami.docker.localhost\"\n"

# new_bp_yaml = "\"version: '2'\n\nservices:\n  whoami:\n    image: emilevauge/whoami\n    environment:\n      - external_hostname=true\n    networks: \n      - traefik-net\n    labels:\n      - \"traefik.backend=whoami\"\n      - \"traefik.frontend.rule=Host:whoami.docker.localhost\"\n\""

# print(json.dumps(app_build_custom_blurprint("te515dst-bp", new_bp_yaml), indent=4, sort_keys=True))

def app_deleat_custom_blueprint(uid):
    result = d.deleteBlueprintById(uid)
    return result

    # print(json.dumps(app_deleat_custom_blueprint('2c9180865ae25e7b015b9ce0503f2779'), indent=4, sort_keys=True))
